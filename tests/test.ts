import {default_restaurant} from '../src/main';
import {Method, process_line, process_lines} from '../src/utils';
import {Restaurant} from '../src/restaurant';


function run_tests(tests: (Method | ((restaurant: Restaurant) => boolean))[]) {
  let restaurant = new Restaurant("default");
  process_lines(restaurant, default_restaurant);
  for(let test of tests) {
    if(typeof test == "function") {
      expect(test(restaurant));
    } else {
        if(test.args) {
          expect(process_line(restaurant, test.command)).toEqual(test.args);
        } else {
           process_line(restaurant, test.command);
        }
    }
  }
}

test('Basic Order Tests', () => {
  let tests: Method[] = [
    {command: "Breakfast 1,2,3", args: "eggs, toast, coffee"},
    {command: "Breakfast 2,3,1", args: "eggs, toast, coffee"},
    {command: "Breakfast 1,2,3,3,3", args: "eggs, toast, coffee(3)"},
    {command: "Breakfast 1", args: "Unable to process: side is missing"},
    {command: "Lunch 1,2,3", args: "sandwich, chips, soda"},
    {command: "Lunch 1,2", args: "sandwich, chips, water"},
    {command: "Lunch 1,1,2,3", args: "Unable to process: sandwich cannot be ordered more than once"},
    {command: "Lunch 1,2,2", args: "sandwich, chips(2), water"},
    {command: "Lunch", args: "Unable to process: main is missing, side is missing"},
    {command: "Dinner 1,2,3,4", args: "steak, potatoes, wine, water, cake"},
    {command: "Dinner 1,2,3", args: "Unable to process: dessert is missing"}
  ];
  run_tests(tests);
});

test('Advanced Order Tests', () => {
  let tests: Method[] = [
    {command: "Breakfast.Side Add Pumpkin", args: "pumpkin was added"},
    {command: "Breakfast 1,3,4", args: "eggs, pumpkin, coffee"},
    {command: "Breakfast 1,2,3,4", args: "Unable to process: multiple sides cannot be ordered"},
    {command: "Lunch Side Add Pumpkin", args: "pumpkin was added"},
    {command: "Lunch 1,3,4", args: "sandwich, pumpkin, soda"},
    {command: "Lunch 1,2,3,4", args: "sandwich, chips, pumpkin, soda"}
  ];
  run_tests(tests);
});
