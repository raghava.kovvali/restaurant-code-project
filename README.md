# Restaurant Project

A project made to simulate ordering from a restaurant menu

## Data Structures
- **Restaurant**, the restaurant. This has a name and a list of **Menus**
    - **Menu** (ex. Breakfast, Lunch, Dinner, Brunch etc.). This has a name and a list of **Categories**
        - **Category** (ex Main, Side, Drink, Appetizer etc.) This has a name, rules, and a list of **Items**
            - **Item** (ex. Toast, Eggs, Chips etc.) This has a name, and a property as to whether or not it allows multiples

## Dependencies and Running the Project
- This project requires nodeJs to run
- Additionally, there are a few node.js dependencies needed to run the project, listed [under the devDependencies in package.json](package.json) these should be automatically be installed upon running [restaurant.sh](restaurant.sh)
- If nodeJs is installed, running `./restaurant.sh` will install all necessary modules and run the project
- `./restaurant.sh help` lists all available commands
- `./restaurant.sh [<txt filename>] [<command_string>]` installs all modules and runs the project
    - If `[<txt filename>]` is specified, the restaurant will be initialized based on the properties of the provided .txt file
    otherwise the default restaurant will be initialized.
    - If `[<command_string>]` is specified, then, upon initializing the file, the given command will be run. ([See Project Commands](## Project Commands)) after which the project will export and close. If no command_string is specified the project will open to receive user input.

## Project Commands
- `list` lists all commands on the project.
- `menus` lists all menus in the Restaurant
-  `export [filename]` outputs restaurant details to filename.txt if unspecified, the restaurant's name is given 
- `close` closes out of the program
- `[<Menu>] print` prints a given menu alongside the indexes for each item. If no menu is specified, all menus are printed
- `Menu <input string>` attempts to order from Menu, where `<input string>` is a list of comma separated numbers representing item indexes
    - ex. `Breakfast 1,2,3`
- `[<Menu>].[<Category>].add <name>` adds an object to the specified layer
    - ex. `add Brunch` adds the menu brunch to the restaurant
    - `Brunch.add Appetizer` adds the category Appetizer to the menu Brunch
    - `Brunch.Appetizer.add Wings` adds the item wings to category appetizer
- `[<Menu>].[<Category>].delete <name>` deletes the object from the specified layer
- `[<Menu>].[<Category>].[<Item>].rename <name> <new name>` renames the object from name to new name
- `<Menu>.<Category> {true | false}` sets whether or not multiple items of the category can be ordered
- `<Menu>.<Category>.<Item> {true | false}` sets whether or not multiple of the item can be ordered
- `<Menu>.<Category> {required | optional | default | always} [<item name>]` sets the category's rule type
    - `required` when ordering from this category's menu, this category is required.
    - `optional` when ordering from this category's menu, this category is optional.
    - `default <name>` when ordering from this category's menu, if no item of this category is specified, name will be ordered
    - `always <name>` when ordering from this category's menu, name will always be ordered
