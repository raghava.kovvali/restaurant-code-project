//Class for noting down Menu Items.
//Stores the name of a given item, and whether or not that item allows multiples
export class Item {
  name: string;
  item_allow_multiples: boolean;

  constructor(name: string, item_allow_multiples?: boolean) {
    this.name = name;
    this.item_allow_multiples = item_allow_multiples || false;
  }
}
