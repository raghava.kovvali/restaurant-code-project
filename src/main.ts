import * as fs from 'fs';
import * as readline from 'readline';
import {Restaurant} from './restaurant';
import {Method, help_desc_maker, process_lines, process_line} from './utils';

export const default_restaurant = "add Breakfast: Main Eggs Required; Side Toast Required; Drink *Coffee Default Water\n"
 + "add Lunch: Main Sandwich Required; *Side Chips Required; Drink Soda Default Water\n"
 + "add Dinner: Main Steak Required; Side Potatoes Required; Drink Wine Always Water; Dessert Cake Required";

function init(command_args: string | undefined): Restaurant {
  let restaurant_text = (command_args ? fs.readFileSync(command_args, 'utf-8') : default_restaurant );
  let restaurant = new Restaurant("Restaurant");
  process_lines(restaurant,restaurant_text);
  return restaurant;
}

function run_from_user_input(restaurant: Restaurant) {
  console.log("Welcome to " + restaurant.name);
  console.log(process_line(restaurant, "menus"));

  var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false,
    prompt: '>'
  });

  rl.on('line', function (line) {
    if(line == "close"){
      rl.close();
      rl.removeAllListeners();
      console.log("thank you for coming!!");
      process.exit(0);
    } else {
      console.log(process_line(restaurant, line.trim()));
    }
  });
}

function main(args: string[]) {
  var restaurant: Restaurant;
  if(args[0] == "help") {
    let mth_arry: Method[] = [
      {command: "node <path to main.js> [<filename>] [<command_string>]", args: "runs the restaurant app.\nIf filename is specified, the restaurant is initialized from \"./files/<filename>\", otherwise a default restaurant is created.\nIf a command string is given, then that command us executed and then the restaurant is exported. Otherwise a process is opened to read user input."},
    ];
    console.log(help_desc_maker(mth_arry));
  } else {
    var command_args = ((args[0] && args[0].includes(".txt")) ? args.shift() : undefined);
      restaurant = init(command_args);
    if(process.argv.length > 0) {
      console.log(process_line(restaurant, process.argv.join(" ")));
      process_line(restaurant,"export" + (command_args || ""));
    } else {
      run_from_user_input(restaurant);
    }
  }
}

process.argv.splice(0,2)
main(process.argv);
