import {Category, category_enum} from "./category";
import {Message_With_Error} from "./utils"

//Menus of type Breakfast, Lunch, Dinner, Happy Hour etc.
export class Menu {
  name: string;
  categories: Category[] = [];

  list(): Category[] {
    return this.categories;
  }

  create_child(command: string, args?: string): string {
      return this.add_category(command, args);
  }

  child_type(): string {
    return "Category";
  }

/*Orders items from the chosen menu by sorting the items by ID, and passing
each subset of items to the menus categories and processing recursively.
Throws an error if any of the categories return errors.
*/
  order(input: number[]): string {
      var args:number[] = [];
      for(let category of this.categories) {
        for(var i = 0; i < category.items.length; i++) {
          args.push(0);
        }
      }
      for(var i of input) {
        args[i-1]++;
      }
      var result: string[] = [];
      var error: string[] = [];
      for(let category of this.categories) {
        let Message_With_Error: Message_With_Error = category.order(args.splice(0,category.items.length));
        if(Message_With_Error.error) {
          error.push(Message_With_Error.value);
        } else {
          result.push(Message_With_Error.value);
        }
      }
      if(error.length > 0) {
        return "Unable to process: " + error.join(", ");
      } else {
        return result.join(", ");
      }
  }


  constructor(name: string, unprocessed_category_list?: string) {
    this.name = name;
    if(unprocessed_category_list !== undefined) {
      var category_strings = unprocessed_category_list.split(';');
      this.add_multiple_categories(category_strings);
    }
  }

  add_multiple_categories(category_strings: string[]) {
    for(var i = 0; i < category_strings.length; i++) {
        this.add_category_by_string(category_strings[i].trim());
    }
  }

  add_category_by_string(category_string: string) {
    let string_args:string[] = category_string.split(" ");
    if(string_args.length < 2) { return;}
    let name = string_args.shift() !;
    let unprocessed_item_list = string_args.shift() !;
    if(string_args.length == 0) {
      this.add_category(name, unprocessed_item_list);
    }
    switch((string_args.shift() !).toLowerCase()) {
      case "required": this.add_category(name, unprocessed_item_list, category_enum.REQUIRED); break;
      case "always": this.add_category(name, unprocessed_item_list, category_enum.ALWAYS, string_args.shift()); break;
      case "default": this.add_category(name, unprocessed_item_list, category_enum.DEFAULT, string_args.shift()); break;
      default: this.add_category(name, unprocessed_item_list, category_enum.OPTIONAL); break;
    }
  }

  add_category(name: string, unprocessed_item_list?: string, nernum?: category_enum, default_item?: string): string {
    var category_name: string;
    var category_allow_multiples: boolean;
    if(name.charAt(0) == '*') {
      category_name = name.substring(1);
      category_allow_multiples = true;
    } else {
      category_name = name;
      category_allow_multiples = false;
    }
    if(this.categories.find(i=>i.name == category_name) === undefined) {
      let category_to_add: Category = new Category(category_name, category_allow_multiples,
        (unprocessed_item_list ? unprocessed_item_list.split(",") : undefined), nernum, default_item);
      this.categories.push(category_to_add);
      return name + " was added successfully";
    }
    return name + " already exists!";
  }

//Prints all of the items in a menu
  print(): string{
    var result:string = this.name + "\n";
    let indent:string = "    ";
    let acc:number = 1;
    for(let category of this.categories) {
      result += indent + category.name + ":\n" + indent;
      switch(category.category_type[0]) {
        case category_enum.REQUIRED: result += "This category is required."; break;
        case category_enum.DEFAULT: result += category.category_type[1] + " will be provided if nothing is ordered."; break;
        case category_enum.ALWAYS: result += category.category_type[1] + " will always be provided."; break;
        default: result += "This category is optional"; break;
      }
      if(category.category_allow_multiples) {
        result += " (multiples are allowed)";
      }
      result += "\n";
      for(let item of category.items) {
        result += indent + indent + acc + ": " + item.name;
        if(item.item_allow_multiples) {
          result += " (multiples are allowed)";
        }
        result += "\n";
        acc++;
      }
      result += "\n";
    }
    return result;
  }

}
