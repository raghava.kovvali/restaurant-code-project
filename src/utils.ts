import * as fs from 'fs';
import {Item} from "./item";
import {Category, category_enum} from "./category";
import {Menu} from "./menu";
import {Restaurant} from "./restaurant";


export const invalid_command = "Invalid command. Type list for a full list of commands";
let mth_arry: Method[] = [
  {command: "<Menu> <Number>,...", args: ["Attempts to order items from Menu with the given indicies.", "Ex: Breakfast 1,2,3",
  "Type <Menu> print for a full list of items and their corresponding indicies on that menu"].join("\n")},
  {command: "menus", args:"Lists all of the menus the restaurant has"},
  {command: "print", args: "Prints all menus in the restaurant."},
  {command: "<Menu> print", args: "Prints the chosen menu."},
  {command: "add <name>", args: "adds a Menu of the given name to the restaurant"},
  {command: "<Menu>.add <name>", args: "adds a Category of the given name to Menu."},
  {command: "<Menu>.<Category>.add <name>", args: "adds an Item of the given name to the Category."},
  {command: "delete <name>", args: "removes Menu name from the the Restaurant"},
  {command: "<Menu>.delete <name>", args: "removes Category name from the Menu."},
  {command: "<Menu>.<Category>.delete <name>", args: "removes Item name from the Category."},
  {command: "rename <name>", args: "changes the name of the Restaurant to name"},
  {command: "<Menu>.rename <name>", args: "changes the name of the Menu to name"},
  {command: "<Menu>.<Category>.rename <name>", args: "changes the name of the Category to name"},
  {command: "<Menu>.<Category>.<Item> rename <name>", args: "changes the name of the Item to name"},
  {command: "<Menu>.<Category> {true | false}", args: "sets the category to allow/disallow multiples"},
  {command: "<Menu>.<Category>.<Item> {true | false}", args: "sets the item to allow/disallow multiples"},
  {command: "<Menu>.<Category> required", args: "sets the category to be required"},
  {command: "<Menu>.<Category> optional", args: "sets the category to be optional"},
  {command: "<Menu>.<Category> default <name>", args: "sets the category to give name if no other items are ordered"},
  {command: "<Menu>.<Category> always <name>", args: "sets the category to always give name"},
  {command: "export [filename]", args: "outputs restaurant details to \"./files/[filename].txt\" if unspecified, the restaurant's name is given"},
  {command: "close", args: "exits out of the program"}
]

export function help_desc_maker(mth_array: Method[]): string {
    return "Usage:\n" + mth_array.map(((mth: Method) => "\t" + mth.command + "\n\t\t" + mth.args!.replace("\n","\n\t\t") + "\n")).join("\n");
}

export const help_desc = help_desc_maker(mth_arry);

//A string with a boolean flag denoting whether or not there is an error
export interface Message_With_Error {
  value: string;
  error: boolean;
}

//A pair of strings, one for the message's command, and one for its arguments
export type Method = {
  command: string,
  args?: string
}

/*checks an array of objects to see if an object with name name exists
Returns the index if successful, or -1 otherwise*/
export function find_by_name(input: any[], name: string): number {
  for(var i = 0; i < input.length; i++) {
    if(input[i].name.toLowerCase() == name) {
      return i;
    }
  }
  return -1;
}

//Parses a string to a "Method" type by splitting on the first space
export function method(input: string): Method {
  var idx = input.indexOf(" ");
  if(idx == -1) {
    return {command: input.toLowerCase().trim()};
  } else {
    return {command: input.substring(0,idx).trim().toLowerCase(), args: input.substring(idx+1).trim().toLowerCase()};
  }
}

/*Converts a string to a Method and passes it as a parameter to a function*/
export function run_as_method(inp: string | undefined, func: (mth: Method) => string, error_message?: string): string{
  if(inp === undefined) {
    return error_message || "missing name";
  } else {
    return func(method(inp));
  }
}

export function call_on_child(val: Restaurant | Menu | Category, input: Method): string {
    let idx = find_by_name(val.list(), input.command);
    if(idx == -1) {
      return invalid_command;
    } else {
      return process_line(val.list()[idx], input.args);
    }
}

export function rename_method(val: Restaurant | Menu | Category, else_condition?: (s: string) => string): (mth: Method) => string {
  return ((mth: Method) => {
    if(mth.args) {
      let idx = find_by_name(val.list(), mth.command);
      if(idx == -1) return "No menu with name " + mth.command + " exists";
      if(find_by_name(val.list(), mth.args) != -1) {
        return "A menu with name " + mth.args + " already exists!";
      }
      val.list()[idx].name = mth.command;
      return val.child_type() + " name was changed to " + val.list();
    } else {
      if(else_condition) {
        return else_condition(mth.command);
      } else {
        return "Missing new name!";
      }
    }
  });
}

export function add_method(val: Restaurant | Menu | Category): (mth: Method) => string {
  return ((mth: Method) => {
      let idx = find_by_name(val.list(), mth.command);
      if(idx == -1) {
        return val.create_child(mth.command,mth.args);
      } else {
        return "A " + val.child_type() + " with name " + mth.command + "already exists!";
      }
    });
}

export function delete_method(val: Restaurant | Menu | Category): (mth: Method) => string {
    return ((mth: Method) => {
        let idx = find_by_name(val.list(), mth.command);
        if(idx == -1) {
          return mth.command + " does not exist";
        } else {
          return val.list().splice(idx,1)[0].name + " was deleted";
        }
      });
}

//Primary switch case, takes a string as input from the command line and parses it
export function process_line(val: Restaurant | Menu | Category | Item, inp: string | undefined): string {
    //Attempts to order if the input is undefined, or is a series of comma separated numbers
    if((!inp || /^[0-9,.]*$/.test(inp)) && val instanceof Menu) {
      if(!inp) return val.order([]);
      return val.order(inp.split(',').map(Number));
    }

    if(val instanceof Item) {
      switch(inp) {
        case "true":
          val.item_allow_multiples = true;
          return val.name + " set to allow multiples";
        case "false":
          val.item_allow_multiples = false;
          return val.name + " set to no longer allow multiples";
        default:
          return invalid_command;
      }
    }

    let input: Method = (inp? method(inp.replace(":","").replace("."," ")) : {command: "print"});
    switch(input.command) {
      case "true":
        if(val instanceof Category) {
          val.category_allow_multiples = true;
          return val.name + " set to allow multiples";
        } else {
          return invalid_command;
        }
      case "false":
        if(val instanceof Category) {
          val.category_allow_multiples = false;
          return val.name + " set to no longer allow multiples";
        } else {
          return invalid_command;
        }
      case "required":
        if(val instanceof Category) {
          val.category_type = [category_enum.REQUIRED];
          return val.name + " set to be required";
        } else {
          return invalid_command;
        }
      case "optional":
        if(val instanceof Category) {
          val.category_type = [category_enum.OPTIONAL];
          return val.name + " set to be optional";
        } else {
          return invalid_command;
        }
      case "default":
        if(val instanceof Category) {
          val.category_type = [category_enum.DEFAULT, input.command];
          return val.name + " set to give " + input.command + "if no item of this category is given";
        } else {
          return invalid_command;
        }
        case "always":
          if(val instanceof Category) {
            val.category_type = [category_enum.ALWAYS, input.command];
            return val.name + " set to always give " + input.command;
          } else {
            return invalid_command;
          }
      case "list":
        if(val instanceof Restaurant) {
            return help_desc;
        } else {
            return invalid_command;
        }
      case "menus":
        if(val instanceof Restaurant) {
            return "Our menus are: " + val.menus.map(i => i.name).join(" ") + "\n Type the name of a menu for a list of items.\n Or type \"list\" for a full list of commands.";
        } else {
            return invalid_command;
        }
      case "print":
        if(val instanceof Restaurant || val instanceof Menu) {
          return val.print();
        } else {
          return invalid_command;
        }
      case "export":
        if(val instanceof Restaurant) {
          let filename: string = (input.args || val.name) + ".txt";
          fs.writeFileSync(filename, val.export());
          return "Data was written to \""+filename+"\". Pass it as a parameter to initialize it when starting up.";
        } else {
          return invalid_command;
        }
      case "rename":
        return run_as_method(input.args, rename_method(val,
          (val instanceof Restaurant ? ((s:string) => {
            val.name = s;
            return "Restaurant name was set to " + val.name;
          })
          : undefined
        )
      ));
      case "add":
        return run_as_method(input.args, add_method(val));
      case "delete":
        return run_as_method(input.args, delete_method(val));
      default:
        return call_on_child(val,input);
    }
}

export function process_lines(restaurant: Restaurant, args: string) {
  for(let arg of args.replace(/\r\n/g,'\n').split('\n')) {
    process_line(restaurant, arg);
  }
}
