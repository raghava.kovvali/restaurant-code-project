import {Item} from "./item";
import {Message_With_Error} from "./utils";

/*Enum listing a category's type
If Optional, this Category can be omitted
If Required, this category must be included when making an ordered
If Default, a string will be returned if this item is empty. For example giving water if no other drink is specified
If Always, like Default, but the named item will always be given*/
export enum category_enum {OPTIONAL = "optional", REQUIRED = "required", DEFAULT = "default", ALWAYS = "always"};

/* Class for Categories such as Main, Side, Pasta, Antipasta etc.*/
export class Category {
  name: string;
  //Stores the Category's type, and the item to give (ex. water) if the category is empty
  category_type: [category_enum, string?];
  //Decides whether to allow multiple items of a given category to be ordered or not
  category_allow_multiples: boolean = false;
  items: Item[] = [];

  child_type(): string {
    return "Item";
  }

  list(): Item[] {
    return this.items;
  }

  create_child(command: string, args?: string): string {
      let item = this.add_item(command);
      switch(args) {
        case "true":
          item.item_allow_multiples = true;
        case "false":
          item.item_allow_multiples = false;
        default:
          break;
      }
      this.list().push(item);
      return command + " was added";
  }

  constructor(name: string, category_allow_multiples: boolean, item_list?: string[], nernum?: category_enum , default_item?: string) {
    this.name = name;
    this.category_allow_multiples = category_allow_multiples;
    this.category_type = [(nernum || category_enum.OPTIONAL), default_item];
    if(item_list) this.add_items(item_list);
  }

  //Returns the name of the given item formatted based on the number ordered
  //The error flag is set to true if this item cannot be ordered in multiples, and this was
  print_item(name: string, num: number, allow_multiples: boolean): Message_With_Error{
    if(num == 0) {
      return {value: "", error:false};
    }
    if(num > 1) {
      if(allow_multiples) {
        return {value: name + "(" + num + ")", error:false};
      } else {
        return {value: name + " cannot be ordered more than once", error: true};
      }
    }
    return {value: name, error:false};
  }

/*Orders items from the given category.
Checks to make sure the conditions for this category's type are met.
Throws error otherwise.
*/
  order(input: number[]): Message_With_Error {
    var result: string[] = new Array;
    var error: string[] = new Array;
    for(var i = 0; i < this.items.length; i++) {
      let response = this.print_item(this.items[i].name, input[i], (this.category_allow_multiples || this.items[i].item_allow_multiples));
      if(response.error) {
        error.push(response.value);
      } else if(response.value !== ""){
        result.push(response.value);
      }
    }
    if(error.length > 0) {
      return {value: error.join(", "), error: true};
    }
    if(this.category_type[0] == category_enum.ALWAYS) {
      result.push(this.category_type[1] || "");
    } else if(result.length == 0) {
      if(this.category_type[0] == category_enum.DEFAULT) {
        result.push(this.category_type[1] || "");
      }
      if(this.category_type[0] == category_enum.OPTIONAL) {
        return {value: "", error: false};
      }
      if(this.category_type[0] == category_enum.REQUIRED) {
        return {value: this.name + " is missing", error:true};
      }
    } else if(result.length > 1 && !this.category_allow_multiples) {
      return {value: "multiple " + this.name + "s cannot be ordered", error:true};
    }
    return {value: result.join(', '), error: false};
  }

  add_item(item: string) {
    if(item.charAt(0) == '*') {
      return {name: item.substring(1), item_allow_multiples: true};
    } else {
      return {name: item, item_allow_multiples: false};
    }
  }

  add_items(item_list: string[]) {
      for(let item of item_list) {
        this.items.push(this.add_item(item));
      }
  }
}
