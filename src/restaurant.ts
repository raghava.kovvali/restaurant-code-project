import {Menu} from "./menu";
import {category_enum} from "./category";

export class Restaurant {
  menus: Menu[] = [];
  name: string;

  list(): Menu[] {
    return this.menus;
  }

  create_child(command: string, args?: string): string {
    let menu = new Menu(command, args);
    this.menus.push(menu);
    return menu.name + " was created";
  }

  child_type(): string {
    return "Menu";
  }

  constructor(name: string) {
    this.name = name;
  }

  //Exports the Restaurant to a model
  export(): string {
    var menu_strings: string[] = [];
    for(let menu of this.menus) {
      var category_strings: string[] = [];
      for(let category of menu.categories) {
        var item_strings: string[] = [];
        for(let item of category.items) {
          let item_result = (item.item_allow_multiples? "*" : "") + item.name;
          item_strings.push(item_result);
        }
        let category_result = (category.category_allow_multiples? "*" : "") + category.name;
        category_result += " " + item_strings.join(",") + " ";
        category_result += category.category_type[0].toString();
        if(category.category_type[0] == category_enum.DEFAULT || category.category_type[0] == category_enum.ALWAYS) {
          category_result += " " + category.category_type[1];
        }
        category_strings.push(category_result);
      }
      let menu_result = "add " + menu.name + ": " + category_strings.join(";");
      menu_strings.push(menu_result);
    }
    return menu_strings.join("\n");
  }

  print() {
    var result = "Our menus are \n";
    for(let menu of this.menus) {
      result += menu.print() + "\n";
    }
    return result;
  }

}
