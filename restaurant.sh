#!/bin/bash
[ ! -d "./lib" ] && npm install && npm run build
[ -d "./lib" ] && node ./lib/main.js $*
